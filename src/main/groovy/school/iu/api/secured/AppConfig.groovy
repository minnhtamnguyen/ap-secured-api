package school.iu.api.secured

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.ldap.core.support.LdapContextSource

@Configuration
class AppConfig {

    @Autowired
    Environment env

    @Bean
    def LdapContextSource contextSource() {
        LdapContextSource contextSource = new LdapContextSource()
        contextSource.setUrl(env.getRequiredProperty("ldap.url"))
        contextSource.setBase(env.getRequiredProperty("ldap.partitionSuffix"))
        contextSource.setUserDn(env.getRequiredProperty("ldap.principal"))
        contextSource.setPassword("secret")
        contextSource
    }
}