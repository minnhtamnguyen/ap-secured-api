package school.iu.api.secured

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.stream.Collectors

import javax.annotation.PostConstruct
import javax.naming.Name

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import com.fasterxml.jackson.databind.ObjectMapper

@Service
class TaskServiceImp implements TaskService{

	@Autowired
	TaskRepository taskRepository
	
//	@PostConstruct
	public void initial(){
		taskRepository.deleteAll()
	}
	
	@Override
	public Task createTask(TaskInput taskInput) {
		Task task = new Task()
		task.setName(taskInput.getName())
		task.setDescription(taskInput.getDescription())
		task.setStartDate(taskInput.getStartDate())
		task.setDeadline(taskInput.getDeadline())
		task.setTaskCode(taskInput.getTaskCode())
		task.setAssignTo(taskInput.getAssignTo())
		task.setProjectCode(taskInput.getProjectCode())
		task.setParentCode(taskInput.getParentCode())
		task.setState(State.PD)
		task.setStatus(Status.OP)
		
		return taskRepository.save(task)
	}

	@Override
	public Task editTask(Object taskInput, Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteTask(Object id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Task approveTask(Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Task rejectTask(Object id) {
		// TODO Auto-generated method stub
		return null;
	}
}

@Service
class ProjectServiceImp implements ProjectService{

	@Autowired
	ProjectRepository projectRepository
	
	@Autowired
	ActivityService activityService
	
	@Autowired
	GroupRepository groupRepository
	
	@Autowired
	UserInfoRepository userInfoRepository
	
	@Autowired
	UserRepository userRepository
	
//	@PostConstruct
	public void initial(){
		projectRepository.deleteAll()
	}
	
	@Override
	public Project createProject(ProjectInput projectInput) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication()
		
		Project project = new Project()
		project.setName(projectInput.getName())
		project.setDescription(projectInput.getDescription())
		project.setProjectCode(projectInput.getProjectCode().toUpperCase())
		project.setStartDate(projectInput.getStartDate())
		project.setDeadline(projectInput.getDeadline())
		project.setState(State.PD)
		project.setStatus(Status.OP)
		def activity = new Activity()
		ObjectMapper mapper = new ObjectMapper()
		project = projectRepository.save(project)
		
		activity.setProjectCode(project.projectCode)
		activity.setFunction("Create Project")
		activity.setDescription("data: " + mapper.writeValueAsString(project))
		activityService.createActivity(activity)
		
		User user = userRepository.findByUsername(auth.getName())
		
		Group group = new Group()
		group.setRole(project.projectCode)
		List<Name> users = new ArrayList<>()
		users.add(user.getId())
		group.setMember(users)	
		groupRepository.save(group)
		
		UserInfo userInfor = userInfoRepository.findOneByUsername(auth.getName())
		userInfor.getProjectCodes().add(project.projectCode)
		userInfoRepository.save(userInfor)
		
		return project
	}

	@Override
	public Project editProject(Object projectInput, Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteProject(Object id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Project approveProject(String id) {
		def project = projectRepository.findById(id).get()
		project.setState(State.AP)
		return projectRepository.save(project);
	}

	@Override
	public Project rejectProject(String id) {
		def project = projectRepository.findById(id).get()
		project.setState(State.DC)
		return projectRepository.save(project)
	}

	@Override
	public List<UserInfo> getMembers(String projectCode) {
		Group group = groupRepository.findOneByRole(projectCode)
		List<UserInfo> users = new ArrayList<>()
		for(Name name: group.getMember()) {
			User user = userRepository.findById(name)
			//users.add(userInfoRepository.findby)
		}
		return userInfoRepository.findByProjectCodes([projectCode]);
	}
}

@Service
class OfficeServiceImp implements OfficeService{

	@Override
	public Office createOffice(Object officeInput) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Office editOffice(Object officeInput, Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteOffice(Object id) {
		// TODO Auto-generated method stub
		return false;
	}
}

@Service
class UserInfoServiceImp implements UserInfoService{
	
	@Autowired
	UserInfoRepository userInfoRepository
	
	@Autowired
	UserService userService
	
	@PostConstruct
	def void initial(){
		userInfoRepository.deleteAll()
		UserInfo user = new UserInfo()
		user.setUsername("admin")
		user.setName("admin")
		user.setProjectCodes(new ArrayList<>())
		userInfoRepository.save(user)
	}

	@Override
	public UserInfo createUserInfo(UserInfoInput userInfoInput) {
		UserInfo userInfor = new UserInfo()
		userInfor.setUsername(userInfoInput.getUsername())
		userInfor.setName(userInfoInput.getName())
		userInfor.setDob(userInfoInput.getDob())
		userInfor.setProjectCodes([userInfoInput.getProjectCode()])
		
		User user = new User()
		user.setUsername(userInfoInput.getUsername())
		user.setFirstName(userInfoInput.getName())
		user.setLastName(userInfoInput.getName())
		user.setPassword(userInfoInput.getPassword())
		userService.create(user)
		userService.addRole(userInfoInput.getUsername(), userInfoInput.getProjectCode())
		
		userInfoRepository.save(userInfor)
	}

	@Override
	public UserInfo editUserInfo(Object userInfoInput, Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteUserInfo(Object id) {
		// TODO Auto-generated method stub
		return false;
	}
}

@Service
class ActivityServiceImp implements ActivityService{

	@Autowired
	ActivityRepository activityRepository
		
	@Override
	public Activity createActivity(Activity activity) {
		return activityRepository.save(activity)
	}

	@Override
	public Activity editActivity(Object activityInput, Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteActivity(Object id) {
		// TODO Auto-generated method stub
		return false;
	}
}

@Service
class UserService {
	@Autowired
	UserRepository userRepository

	@Autowired
	GroupRepository groupRepository

	Boolean authenticate(final String username, final String password) {
		User user = userRepository.findByUsernameAndPassword(username, password)
		user != null
	}

	User create(User user) {
		user.setPassword(digestSHA(user.getPassword()))
		user = userRepository.save(user)
		user
	}

	List<String> search(final String username) {
		List<User> userList = userRepository.findByUsernameLikeIgnoreCase(username);
		if (userList == null) {
			return Collections.emptyList();
		}

		return userList.stream()
		  .map({u -> u.getUsername()})
		  .collect(Collectors.toList());
	}
	
	Group addRole(final String username, final String role) {
		if(!search(username).isEmpty()) {
			Group group = groupRepository.findOneByRole(role)
			User user = userRepository.findOneByUsernameLikeIgnoreCase(username)
			List<Name> users = group.getMember()
			users.add(user.getId())
			return groupRepository.save(group)
		}
		null
	}

	void modify(final String username) {
		User user = userRepository.findByUsername(username)
		userRepository.save(user)
	}

	String digestSHA(final String password) {
		String base64
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA")
			digest.update(password.getBytes())
			base64 = Base64.getEncoder()
					.encodeToString(digest.digest())
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e)
		}
		"{SHA}" + base64
	}
}

@Service
class GroupService {
	@Autowired
	GroupRepository groupRepository

	@Autowired
	UserRepository userRepository

	List<String> findByRole(final String role) {
		Group userList = groupRepository.findOneByRole(role)
		List<String> usernames = new ArrayList<>()
		for(Name name: userList.getMember()) {
			userRepository.findById(name)
			usernames.add(userRepository.findById(name).get().getUsername())
		}
		usernames
	}
}