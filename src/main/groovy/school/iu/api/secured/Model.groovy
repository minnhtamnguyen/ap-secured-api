package school.iu.api.secured

import javax.naming.Name

import org.springframework.context.annotation.Configuration
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.domain.AuditorAware
import org.springframework.data.ldap.repository.LdapRepository
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.ldap.odm.annotations.Attribute
import org.springframework.ldap.odm.annotations.DnAttribute
import org.springframework.ldap.odm.annotations.Entry
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Repository

import com.fasterxml.jackson.annotation.JsonFormat

enum Status{
	OP, PR, CP
}

enum State{
	AP, DC, PD
}

@Document
class UserInfo {
	@Id
	String id
	String username
	String name
	Date dob
	String phone
	String officeCode
	List<String> projectCodes
}

class UserInfoInput {
	String name
	String username
	String password
	String projectCode
	Date dob
	String phone
}

interface UserInfoRepository extends PagingAndSortingRepository<UserInfo, String> {
	def List<UserInfo> findByOfficeCode(String officeCode)
	def UserInfo findOneByUsername(String username)
	def List<UserInfo> findByProjectCodes(List<String> projectCodes)
}

@Document
class Project{
	@Id
	String id
	String name
	String description
	Date startDate
	Status status
	State state
	@CreatedDate
	Date createdDate
	Date deadline
	String projectCode
	@CreatedBy
	String createdBy
}

class ProjectInput {
	String name
	String description
	String projectCode
	Date startDate
	Date deadline
}

interface ProjectRepository extends PagingAndSortingRepository<Project, String> {
	def List<Project> findByCreatedBy(String createdBy)
	def Project findByProjectCode(String projectCode)
}

@Document
class Task{
	@Id
	String id
	String name
	String description
	Date startDate
	Date deadline
	Status status
	State state
	String taskCode
	@CreatedDate
	Date createdDate
	String projectCode
	String assignTo
	String reporter
	String parentCode
}

class TaskInput{
	String name
	String description
	String taskCode
	Date startDate
	Date deadline
	String projectCode
	String assignTo
	String reporter
	String parentCode
}

interface TaskRepository extends PagingAndSortingRepository<Task, String> {
	def List<Task> findByProjectCode(String projectCode)
	def List<Task> findByAssignTo(String assignTo)
	def List<Task> findByReporter(String reporter)
	def List<Task> findByParentCode(String ParentCode)
	def Task findByTaskCode(String taskCode)
}

@Document
class Office{
	@Id
	String id
	String name
	String officeCode
	@CreatedDate
	Date createdDate
}

class OfficeInput {
	String name
	String officeCode
}


interface OfficeRepository extends PagingAndSortingRepository<Office, String> {
	def List<Office> findByCreatedDate(Date createdDate)
}

@Document
class Activity{
	@Id
	String id
	String description
	String function
	@CreatedDate
	Date createdDate
	String userId
	String projectCode
	String taskCode
	
}

class ActivityInput {
	String description
	String function	
}

interface ActivityRepository extends PagingAndSortingRepository<Activity, String> {
	def List<Activity> findByUserId(String userId)
	def List<Activity> findByProjectCode(String projectCode)
	def List<Activity> findByTaskCode(String taskCode)
}

@Configuration
public class AuditorAwareImpl implements AuditorAware<String>{

	@Override
	public Optional<String> getCurrentAuditor() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.empty();
        }

        return Optional.of(authentication.getName());
	}
}



@Entry(base="ou=Groups,dc=example,dc=com", objectClasses = ["groupOfNames", "top"])
public class Group {
	@org.springframework.ldap.odm.annotations.Id
	Name id

	@DnAttribute(value="cn", index=3) @Attribute(name="cn") String role
	List<Name> member
}

@Entry(base="ou=Users,dc=example,dc=com", objectClasses = [ "person", "inetOrgPerson", "organizationalPerson", "top" ])
class User{
	@org.springframework.ldap.odm.annotations.Id
	Name id
	
	@DnAttribute(index= 3, value = "uid") @Attribute(name="uid") String username
	
	@Attribute(name="userPassword") String password

	@Attribute(name="cn") String lastName
	
	@Attribute(name="sn") String firstName
	
	String employeeNumber
	
	List<String> telephoneNumber
}
interface GroupRepository extends LdapRepository<Group> {
	Group findOneByRole(String role)
}

interface UserRepository extends LdapRepository<User> {
	User findByUsername(String username)
	User findByUsernameAndPassword(String username, String password)
	List<User> findByUsernameLikeIgnoreCase(String username)
	User findOneByUsernameLikeIgnoreCase(String username)
}
